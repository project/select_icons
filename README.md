# Select Icons

This module provides a Form API select element with icons using jQuery UI.

This module is for developers only. It does not provide any user functionality.

With this module you can easily create select element with icons of your choice
using nothing but drupal Forms API and some CSS:

```php
$form['fancy_select'] = [
  '#type' => 'select_icons',
  '#title' => $this->t('Color'),
  '#options' => [
    'r' => $this->t('Red'),
    'g' => $this->t('Green'),
    'b' => $this->t('Blue'),
  ],
  // This is where magic happens:
  // CSS classes from 'data-class' attribute are used in JavaScript
  // when building jQuery UI selectmenu widget.
  '#options_attributes' => [
    'r' => new Attribute(['data-class' => ['color', 'red']]),
    'g' => new Attribute(['data-class' => ['color', 'green']]),
    'b' => new Attribute(['data-class' => ['color', 'blue']]),
  ],
  // Don't forget to add proper CSS that will provide icons.
  // It is recommended to use sprite sheets for better performance.
  '#attached' => [ 'library' => ['my/colors'] ],
];
```


## Requirements

This module requires the following modules:
- [jQuery UI Selectmenu Widget](https://www.drupal.org/project/jquery_ui_selectmenu)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
