(($, Drupal) => {
  $.widget('custom.selecticons', $.ui.selectmenu, {
    _renderItem(ul, item) {
      const li = $('<li>', { text: item.label });

      if (item.disabled) {
        li.addClass('ui-state-disabled');
      }

      $('<span>', {
        style: item.element.attr('data-style'),
        class: item.element.attr('data-class'),
      }).appendTo(li);

      return li.appendTo(ul);
    },
  });

  Drupal.behaviors.select_icons = {
    attach(context) {
      const $selecticons = $('.selecticons', context);
      if (!$selecticons.length) {
        return;
      }

      $selecticons.selecticons({
        create() {
          // Get classes of currently selected option.
          const currentClasses = $(this)
            .find('option:selected')
            .attr('data-class');
          const button = $(`#${this.id}-button`);

          // Create element for current selection's icon.
          button.prepend(
            `<span class="ui-current-item-icon ${currentClasses}"></span>`,
          );
        },
        select(event, ui) {
          // Get current option's icon element.
          const currentIcon = $(`#${this.id}-button > .ui-current-item-icon`);

          // Get selected option's classes.
          const classes = ui.item.element.attr('data-class');

          // Set new classes for current option's icon element.
          currentIcon.removeClass();
          currentIcon.addClass(`${classes} ui-current-item-icon`);
        },
        change() {
          $(this).trigger('change');
        },
        width: 200,
      });

      const display = window.getComputedStyle($selecticons[0]).direction;
      if (display === 'rtl') {
        $selecticons.selecticons({
          position: {
            my: 'right top',
            at: 'right bottom',
            collision: 'none',
          },
        });
      }
    },
  };
})(jQuery, Drupal);
