<?php

declare(strict_types=1);

namespace Drupal\Tests\select_icons\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the JavaScript select_icons - jquery selectmenu widget.
 *
 * @group select_icons
 */
class SelectIconsElementIntegrationTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'select_icons',
    'select_icons_test',
  ];

  /**
   * Test select icons element.
   */
  public function testSelectIconsRenderedFormElement(): void {
    $this->drupalGet('select_icons_test/form_element');

    $page = $this->getSession()->getPage();

    // Check if selecticons select is hidden.
    /** @var \Behat\Mink\Element\NodeElement $selecticons_select */
    $selecticons_select = $page->find('css', 'select#select-icons-test-element');
    $this->assertFalse($selecticons_select->isVisible(), 'Selectmenu select is hidden.');

    // Check if selecticons select has options with data attribute.
    /** @var \Behat\Mink\Element\NodeElement $option_red */
    $option_red = $page->find('css', 'select#select-icons-test-element option[value="r"]');
    $this->assertEquals('color red', $option_red->getAttribute('data-class'));

    // Check if selectmenu is opened.
    /** @var \Behat\Mink\Element\NodeElement $selecticons_widget */
    $selecticons_widget = $page->find('css', '.js-form-type-select-icons .ui-selectmenu-button');
    $selecticons_widget->click();
    $selecticons_widget->find('css', 'ui-selectmenu-open');
    $this->assertTrue($selecticons_widget->isVisible(), 'Selectmenu is visible.');
  }

}
