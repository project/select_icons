<?php

declare(strict_types=1);

namespace Drupal\select_icons_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Form constructor for testing #type 'select_icons' element.
 */
class SelectIconsFormElementTestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'select_icons_form_element_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['select_icons_select'] = [
      '#type' => 'select_icons',
      '#attributes' => ['id' => 'select-icons-test-element'],
      '#title' => $this->t('Color'),
      '#options' => [
        'r' => $this->t('Red'),
        'g' => $this->t('Green'),
        'b' => $this->t('Blue'),
      ],
      '#options_attributes' => [
        'r' => new Attribute(['data-class' => ['color', 'red']]),
        'g' => new Attribute(['data-class' => ['color', 'green']]),
        'b' => new Attribute(['data-class' => ['color', 'blue']]),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setResponse(new JsonResponse($form_state->getValues()));
  }

}
